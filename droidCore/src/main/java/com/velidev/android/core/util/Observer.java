package com.velidev.android.core.util;

public interface Observer {
	
	public void update(int code, Object data);

}
