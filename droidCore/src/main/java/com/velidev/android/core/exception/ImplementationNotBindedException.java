package com.velidev.android.core.exception;

public class ImplementationNotBindedException extends RuntimeException {

	private static final long serialVersionUID = 5759598695058759152L;
	private Class<?> clazz;
	
	public ImplementationNotBindedException(Class<?> clazz) {
		this.clazz = clazz;
	}
	
	@Override
	public String getMessage() {
		return "Implementation of class " + clazz.getName() + " not binded";
	}
	
}
