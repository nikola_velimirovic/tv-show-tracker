package com.velidev.android.core.db.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.velidev.android.core.db.ColumnType;
import com.velidev.android.core.db.CoreDatabase;
import com.velidev.android.core.object.CachableObject;

public abstract class AbstractDao<T extends CachableObject> {
	
	public static final String ID = "_id";
	protected CoreDatabase database;
	
	protected abstract String getTableName();
	protected abstract T createObject(Cursor c);
	protected abstract ContentValues createCV(T object);
	
	public AbstractDao(CoreDatabase db) {
		this.database = db;
	}
	
	protected long insert(T object) {
		long result = database.getWritableDatabase().insert(getTableName(), null, createCV(object));
		if(result==-1) {
			Log.e("Insert object", "Error inserting object");
		} else {
			Log.d("Insert object", "Object inserted successfully, ID: " + result);
		}
		return result;
	}
	
	protected void update(T object) {
		if(object.getIdAndroid()==null) {
			Log.w("Update object", "Updating object with null ID, inserting instead");
			insert(object);
			return;
		} else {
			int rowsAffected = database.getWritableDatabase().update(getTableName(), createCV(object), ID+"=?", new String[]{object.getIdAndroid()+""});
			Log.d("Update object", "Update row(s) affected " + rowsAffected);
		}
	}
	
	protected void delete(T object) {
		int rowsAffected = database.getWritableDatabase().delete(getTableName(), ID+"=?", new String[] {object.getIdAndroid()+""});
		Log.d("Delete object", "Delete row(s) affected " + rowsAffected);
	}
	
	protected T get(Long id) {
		Cursor c = database.getReadableDatabase().rawQuery("SELECT * FROM " + getTableName() + " WHERE "+ID+"=?", new String[] {id+""});
		if(c.moveToFirst()) {
			Log.d("Get object", "Object by ID " + id +" found");
			return createObject(c);
		}
		else {
			Log.w("Get object", "Object by ID " + id + " not found");
			return null;
		}
	}
	
	protected List<T> getAll() {
		Cursor c = database.getReadableDatabase().rawQuery("SELECT * FROM " + getTableName(), new String[]{});
		return get(c);
	}
	
	protected List<T> get(Cursor c) {
		Log.d("Get objects", "Found " + c.getCount() + " record(s)");
		if(c.moveToFirst()) {
			List<T> result = new LinkedList<T>();
			do {
				T object = createObject(c);
				result.add(object);
			} while(c.moveToNext());
			return result;
		} else return new LinkedList<T>();
	}
	
	protected List<T> get(String query, String...args) {
		Log.d("Custom Query", query);
		if(args!=null) Log.d("Parameters", args.toString());
		Cursor c = database.getReadableDatabase().rawQuery(query, args);
		return get(c);
	}
	
	protected int getCount(String query, String...args) {
		Log.d("Custom Query", query);
		if(args!=null) Log.d("Parameters", args.toString());
		Cursor c = database.getReadableDatabase().rawQuery(query, args);
		return c.getCount();
	}
	
	protected T getFirst(String query, String...args) {
		Cursor c = database.getReadableDatabase().rawQuery(query, args);
		if(c.moveToFirst()) return createObject(c);
		Log.w("Get first object", "Object with query " + query + " not found");
		return null;
	}
	
	protected int countAll() {
		return count("SELECT " + ID + " FROM " + getTableName());
	}
	
	protected int count(String query, String...args) {
		Cursor c = database.getReadableDatabase().rawQuery(query, args);
		return c.getCount();
	}
	
	protected abstract Map<String, ColumnType> buildTableColumns();
	
	public String buildCreateQuery() {
		StringBuilder query = new StringBuilder("CREATE TABLE " + getTableName() + " ( " +
				ID + " INTEGER PRIMARY KEY AUTOINCREMENT, ");
		Map<String, ColumnType> columns = buildTableColumns();
		List<String> columnNames = new LinkedList<String>(columns.keySet());
		for(int i = 0; i < columns.size(); i++) {
			String key = columnNames.get(i);
			query.append(key + " " + columns.get(key).name());
			if(i == columns.size()-1) query.append(")");
			else query.append(", ");
		}
		Log.d("Create Query", query.toString());
		return query.toString();
	}

}
