package com.velidev.android.core.db;

import java.util.List;

import com.velidev.android.core.object.CachableObject;

public interface BaseDao<T extends CachableObject> {
	
	public void save(T object);
	public void delete(T object);
	public T get(Long id);
	public void update(T object);
	public List<T> get();

}
