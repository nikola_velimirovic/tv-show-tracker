package com.velidev.android.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class CoreDatabase extends SQLiteOpenHelper {
	
	public CoreDatabase(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

}
