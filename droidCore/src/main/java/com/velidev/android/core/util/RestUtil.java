package com.velidev.android.core.util;

import org.springframework.web.client.RestTemplate;

public class RestUtil {

    private static RestTemplate rest;
    public static RestTemplate getRestClient() {
        if(rest==null) {
            rest = new RestTemplate();
        }
        return rest;
    }

}
