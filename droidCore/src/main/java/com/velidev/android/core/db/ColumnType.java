package com.velidev.android.core.db;

public enum ColumnType {
	
	INTEGER,
	TEXT,
	NULL,
	REAL,
	BLOB

}
