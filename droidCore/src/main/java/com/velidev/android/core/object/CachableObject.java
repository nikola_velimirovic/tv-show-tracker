package com.velidev.android.core.object;

public abstract class CachableObject {
	
	protected Long idAndroid;

	public Long getIdAndroid() {
		return idAndroid;
	}

	public void setIdAndroid(Long idAndroid) {
		this.idAndroid = idAndroid;
	}
	
}
