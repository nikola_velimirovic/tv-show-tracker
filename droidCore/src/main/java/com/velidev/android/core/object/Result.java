package com.velidev.android.core.object;

/**
 * Created by Nikola on 23.09.2014.
 */
public class Result<T> {

    private T result;
    private boolean success;
    private String message;
    private boolean retry;

    public Result() {
    }

    public Result(T result) {
        this.result = result;
    }

    public Result(boolean success, T result) {
        this.success = success;
        this.result = result;
    }

    public T getResult() {
        return result;
    }
    public void setResult(T result) {
        this.result = result;
    }
    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public boolean isRetry() {
        return retry;
    }
    public void setRetry(boolean retry) {
        this.retry = retry;
    }

    public Result<T> success(boolean success) {
        this.success = success;
        return this;
    }

    public Result<T> result(T result) {
        this.result = result;
        return this;
    }

    public Result<T> message(String message) {
        this.message = message;
        return this;
    }

    public Result<T> retry(boolean retry) {
        this.retry = retry;
        return this;
    }
}
