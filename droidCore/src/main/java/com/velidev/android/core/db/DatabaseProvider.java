package com.velidev.android.core.db;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.velidev.android.core.app.AbstractApp;

public class DatabaseProvider {
	
	private Map<Class<?>, BaseDao<?>> daos;
	
	private static DatabaseProvider instance;
	public static DatabaseProvider getInstance() {
		if(instance==null) {
			synchronized (DatabaseProvider.class) {
				if(instance==null) {
					instance = new DatabaseProvider();
				}
			}
		}
		return instance;
	}
	
	private DatabaseProvider() {
		
	}
	
	public <T extends BaseDao<?>> T getDao(Class<T> clazz, CoreDatabase database) {
		if(daos==null) daos = new HashMap<Class<?>, BaseDao<?>>();
		if(daos.containsKey(clazz)) return clazz.cast(daos.get(clazz));
		T instance = createInstance(clazz, database);
		daos.put(clazz, instance);
		return instance;
	}
	
	private <T extends BaseDao<?>> T createInstance(Class<T> clazz, CoreDatabase database) {
		Class<T> implementationClass = AbstractApp.instance.getBinder().getImplementingClass(clazz);
		try {
			Log.d("Implementing class", implementationClass.getName());
			return implementationClass.getConstructor(CoreDatabase.class).newInstance(database);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}

}
