package com.velidev.android.core.app;

import java.util.HashMap;
import java.util.Map;

import com.velidev.android.core.db.BaseDao;
import com.velidev.android.core.exception.ImplementationNotBindedException;

public class Binder {
	
	private Map<Class<?>, Class<?>> binderMap;
	
	public Binder() {
		binderMap = new HashMap<Class<?>, Class<?>>();
	}
	
	public <T extends BaseDao<?>, I extends T> void bind(Class<T> declaration, Class<I> implementation) {
		binderMap.put(declaration, implementation);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends BaseDao<?>, I extends T> Class<I> getImplementingClass(Class<T> declaration) {
		if(binderMap.containsKey(declaration)) return (Class<I>) binderMap.get(declaration);
		throw new ImplementationNotBindedException(declaration);
	}

}
