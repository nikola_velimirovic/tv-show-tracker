package com.velidev.android.core.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.velidev.android.core.R;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.model.ImageTagFactory;

public abstract class AbstractApp extends Application {
	
	private Binder binder;
    private SharedPreferences preferences;
	public static AbstractApp instance;
	
	public AbstractApp() {
		binder = new Binder();
		bind(binder);
		instance = this;
	}
	
	protected abstract void bind(Binder binder);
	public Binder getBinder() {
		return binder;
	}
	
	public static ImageManager getImageManager(Context context) {
		return new ImageManager(context, createLoaderSettings(context));
	}
	
	public static ImageTagFactory getImageTagFactory(Context context) {
		ImageTagFactory factory = ImageTagFactory.newInstance(context, R.drawable.loading_image);
		factory.setErrorImageId(R.drawable.no_image);
		return factory;
	}
	
	private static LoaderSettings createLoaderSettings(Context context) {
		LoaderSettings settings = new SettingsBuilder()
		.withDisconnectOnEveryCall(true)
		.withConnectionTimeout(20000)
		.withReadTimeout(30000)
		.build(context);
		
		return settings;
	}

    public SharedPreferences getPreferences() {
        if(preferences==null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(this);
        }
        return preferences;
    }


}
