package com.velidev.android.core.util;

import java.util.ArrayList;
import java.util.List;

public class Observable {
	
	private static Observable instance;
	public static Observable getInstance() {
		if(instance==null) {
			instance = new Observable();
		}
		return instance;
	}
	
	private Observable() {
		observers = new ArrayList<Observer>();
	}
	
	private List<Observer> observers;

	public void update(int code, Object data) {
		for (Observer observer : observers) {
			observer.update(code, data);
		}
	}
	
	public void register(Observer obs) {
		observers.add(obs);
	}
	
	public void unregister(Observer obs) {
		observers.remove(obs);
	}
	
	public void clear() {
		observers.clear();
	}

}
