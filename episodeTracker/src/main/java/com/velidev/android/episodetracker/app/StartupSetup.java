package com.velidev.android.episodetracker.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.velidev.android.episodetracker.MainActivity;
import com.velidev.android.episodetracker.R;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.service.CheckForReleasedEpisodeService;
import com.velidev.android.episodetracker.task.ShowDetailsTask;
import com.velidev.android.episodetracker.util.Const;

import java.util.Calendar;

/**
 * Created by Nikola on 22.9.14..
 */
public class StartupSetup {

    private App context;
    private MainActivity activity;
    private AlarmManager alarmManager;

    public StartupSetup(MainActivity activity) {
        this.context = activity.getAppContext();
        this.activity = activity;
    }

    public void execute() {
        syncShowsDataIfNeeded();
        scheduleReleaseDatesCheck();
    }

    private long getScheduleTimestamp() {
        int notificationHour = context.getPreferences().getInt("release.date.check.hour", 12);
        int notificationMinute = context.getPreferences().getInt("release.date.check.minute", 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, notificationHour);
        calendar.set(Calendar.MINUTE, notificationMinute);
        return calendar.getTimeInMillis();
    }

    private PendingIntent createReleaseDateCheckPI() {
        Intent intent = new Intent(context, CheckForReleasedEpisodeService.class);
        PendingIntent pi = PendingIntent.getService(context, 0, intent, 0);
        return pi;
    }

    private void scheduleReleaseDatesCheck() {
        PendingIntent pendingIntent = createReleaseDateCheckPI();
        getAlarmManager().cancel(pendingIntent);
        getAlarmManager().setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getScheduleTimestamp(),
                Const.ONE_DAY_MILIS,
                pendingIntent);
    }

    private AlarmManager getAlarmManager() {
        if(alarmManager==null) {
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        return alarmManager;
    }

    private void syncShowsDataIfNeeded() {
        if(shouldSynchronize()) {
            Log.d("StartupSetup", "Running synchronization");
            runSynchronization();
        } else Log.d("StartupSetup", "No need for sync");
    }

    private boolean shouldSynchronize() {
        if(context.getPreferences().contains(Const.PROP_LAST_SYNC)) {
            long lastSyncTimestamp = context.getPreferences().getLong(Const.PROP_LAST_SYNC, 0);
            if (System.currentTimeMillis() - lastSyncTimestamp < Const.ONE_DAY_MILIS) return false;
        }
        return true;
    }

    private void runSynchronization() {
        Toast.makeText(context, R.string.synchronizing, Toast.LENGTH_SHORT).show();
        Show[] shows = new Show[activity.getListAdapter().getCount()];
        for (int i = 0; i < shows.length; i++) {
            shows[i] = activity.getListAdapter().getItem(i);
        }
        new ShowDetailsTask(context).execute(shows);
        markSynchronized();
    }

    private void markSynchronized() {
        SharedPreferences.Editor editor = context.getPreferences().edit();
        editor.putLong(Const.PROP_LAST_SYNC, System.currentTimeMillis());
        editor.apply();
    }

}
