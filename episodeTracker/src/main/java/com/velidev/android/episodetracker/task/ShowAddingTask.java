package com.velidev.android.episodetracker.task;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.velidev.android.episodetracker.R;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.db.ShowsDao;
import com.velidev.android.episodetracker.declaration.DataSetChangeable;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.util.Const;
import com.velidev.android.episodetracker.util.RestHelper;
import com.velidev.android.core.db.DatabaseProvider;
import com.velidev.android.core.object.Result;
import com.velidev.android.core.util.RestUtil;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

public class ShowAddingTask extends AsyncTask<String, Void, Result<JsonObject>> {
	
	private DataSetChangeable dataSetChangable;
    private ShowsDao showDao;
    private String url;

	public ShowAddingTask(DataSetChangeable dataSet) {
		this.dataSetChangable = dataSet;
	}

	@Override
	protected Result<JsonObject> doInBackground(String... params) {
        url = Const.REST_API_URL + params[0] + "/info/";
        Result<JsonObject> result = new Result<JsonObject>();
        try {
            JsonObject response = RestUtil.getRestClient().getForObject(url, JsonObject.class);
            return result.success(true).result(response);
        } catch (ResourceAccessException e) {
            Log.e("ShowAddingTask", "Error fetching show info, retry...", e);
            return result.success(false).message(e.getMessage()).retry(true);
        } catch (HttpClientErrorException e) {
            Log.d("ShowAddingTask", e.getMessage());
            return result.success(false).message("Show not found").retry(false);
        }
	}
	
	@Override
	protected void onPostExecute(Result<JsonObject> result) {
        if(result.isSuccess()) {
            handleShowInfo(result.getResult());
        } else {
            Log.e("Adding show", result.getMessage());
            if(result.isRetry()) {
                Log.d("Adding show", "Retring...");
                new ShowAddingTask(dataSetChangable).execute(url);
            } else {
                Log.d("Adding show", "Not retring");
                Toast.makeText(App.instance, result.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

	}
	
    private void handleShowInfo(JsonObject response) {
        Show show = RestHelper.convertShow(response);
        if(show!=null) {
            getShowDao().save(show);
            if(dataSetChangable!=null) dataSetChangable.notifyDataSetChanged();
            Toast.makeText(App.instance, R.string.show_saved, Toast.LENGTH_SHORT).show();
        }
    }

    private ShowsDao getShowDao() {
        if(showDao == null) {
            showDao = DatabaseProvider.getInstance().getDao(ShowsDao.class, App.getDatabase());
        }
        return showDao;
    }

}
