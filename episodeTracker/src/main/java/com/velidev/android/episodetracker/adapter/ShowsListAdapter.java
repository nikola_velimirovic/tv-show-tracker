package com.velidev.android.episodetracker.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.velidev.android.episodetracker.R;
import com.velidev.android.episodetracker.ShowDetailsActivity;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.db.EpisodesDao;
import com.velidev.android.episodetracker.db.ShowsDao;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.util.Const;
import com.velidev.android.core.db.DatabaseProvider;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class ShowsListAdapter extends ArrayAdapter<Show> {
	
	private LayoutInflater inflater;
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private EpisodesDao episodesDao;
	
	public ShowsListAdapter(Context context) {
		super(context, R.layout.list_item_show, DatabaseProvider.getInstance().getDao(ShowsDao.class, App.getDatabase()).get());
	}
	
	public void notifyDataChanged() {
		clear();
		addAll(DatabaseProvider.getInstance().getDao(ShowsDao.class, App.getDatabase()).get());
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = createListItemView(convertView);
		Show show = getItem(position);
		view.setOnClickListener(createOnClickListener(show));
		initShowThumb(view, show);
		initShowTitle(view, show);
		initSeenAllBadge(view, show);
		return view;
	}
	
	@Override
	public Show getItem(int position) {
		int index = getCount()-position-1;
		return super.getItem(index);
	}
	
	private View createListItemView(View convertView) {
		if(convertView!=null) return convertView;
		else return getLayoutInflater().inflate(R.layout.list_item_show, null);
	}
	
	private LayoutInflater getLayoutInflater() {
		if(inflater==null) inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return inflater;
	}
	
	private void initShowThumb(View view, Show show) {
		ImageView thumb = (ImageView) view.findViewById(R.id.showThumb);
		thumb.setTag(getImageTagFactory().build(buildUrl(show), getContext()));
		getImageManager().getLoader().load(thumb);
	}
	
	private void initShowTitle(View view, Show show) {
		TextView title = (TextView) view.findViewById(R.id.showTitle);
		title.setText(show.getTitle());
	}
	
	private void initSeenAllBadge(View view, Show show) {
		ImageView badge = (ImageView) view.findViewById(R.id.seen_badge);
		boolean hasEpisodes = getEpisodesDao().hasAtLeastOnEpisodeSaved(show.getIdAndroid());
		boolean seenAll = getEpisodesDao().hasSeenAllEpisodesOfShow(show.getIdAndroid());
		if(!hasEpisodes || !seenAll) 
			badge.setVisibility(View.INVISIBLE);
		else badge.setVisibility(View.VISIBLE);
	}
	
	private ImageManager getImageManager() {
		if(imageManager == null) imageManager = App.getImageManager(getContext());
		return imageManager;
	}
	
	private ImageTagFactory getImageTagFactory() {
		if(imageTagFactory == null) imageTagFactory = App.getImageTagFactory(getContext());
		return imageTagFactory;
	}
	
	private String buildUrl(Show show) {
		return Const.SERVER_URL + show.getEpguideName() + "/cast.jpg";
	}
	
	private View.OnClickListener createOnClickListener(final Show show) {
		return new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent showSelection = new Intent(getContext(), ShowDetailsActivity.class);
				showSelection.putExtra(ShowDetailsActivity.SHOW_ID, show.getIdAndroid());
				getContext().startActivity(showSelection);
			}
		};
	}
	
	private EpisodesDao getEpisodesDao() {
		if(episodesDao==null) episodesDao = DatabaseProvider.getInstance().getDao(EpisodesDao.class, App.getDatabase());
		return episodesDao;
	}

}
