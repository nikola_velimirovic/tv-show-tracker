package com.velidev.android.episodetracker.object;

import com.velidev.android.core.object.CachableObject;

import java.util.Date;

public class Episode extends CachableObject {
	
	private String epguideName;
	private String episodeName;
	private int season;
	private int episode;
	private Date releaseDate;
	private boolean watched;
	private Long showId;

	public String getEpguideName() {
		return epguideName;
	}
	public void setEpguideName(String epguideName) {
		this.epguideName = epguideName;
	}
	public String getEpisodeName() {
		return episodeName;
	}
	public void setEpisodeName(String episodeName) {
		this.episodeName = episodeName;
	}
	public int getSeason() {
		return season;
	}
	public void setSeason(int season) {
		this.season = season;
	}
	public int getEpisode() {
		return episode;
	}
	public void setEpisode(int episode) {
		this.episode = episode;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public boolean isWatched() {
		return watched;
	}
	public void setWatched(boolean watched) {
		this.watched = watched;
	}
	public Long getShowId() {
		return showId;
	}
	public void setShowId(Long showId) {
		this.showId = showId;
	}
	public boolean isReleased() {
		if(releaseDate!=null) {
			return releaseDate.getTime() <= System.currentTimeMillis();
		}
		return false;
	}

	public enum JsonField {
		season,
		number,
		title,
		release_date,
		show
	}
	
}
