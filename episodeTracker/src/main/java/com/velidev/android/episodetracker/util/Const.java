package com.velidev.android.episodetracker.util;

public class Const {
	
	public static final String SERVER_URL = "http://epguides.com/";
	public static final String REST_API_URL = "http://epguides.frecar.no/show/";
	
	public static final String JSON_ERROR = "error";
	
	public static final String UTF_8 = "UTF-8";
    public static final String PROP_LAST_SYNC = "last.sync";
    public static final int ONE_DAY_MILIS = 86400000;

}
