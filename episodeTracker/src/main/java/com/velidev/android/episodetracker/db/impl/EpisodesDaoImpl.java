package com.velidev.android.episodetracker.db.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;

import com.velidev.android.episodetracker.db.EpisodesDao;
import com.velidev.android.episodetracker.object.Episode;
import com.velidev.android.episodetracker.util.Const;
import com.velidev.android.core.db.ColumnType;
import com.velidev.android.core.db.CoreDatabase;
import com.velidev.android.core.db.impl.AbstractDao;

public class EpisodesDaoImpl extends AbstractDao<Episode> implements EpisodesDao {

    public static final String EPGUIDE_NAME = "epguideName";
    public static final String EPISODE_NAME = "episodeName";
    public static final String SEASON = "season";
    public static final String EPISODE = "episode";
    public static final String RELEASE_DATE = "releaseDate";
    public static final String WATCHED = "watched";
    public static final String SHOW_ID = "showId";

    public EpisodesDaoImpl(CoreDatabase db) {
        super(db);
    }

    @Override
    public void save(Episode object) {
        super.insert(object);
    }

    @Override
    public void delete(Episode object) {
        super.delete(object);
    }

    @Override
    public Episode get(Long id) {
        return super.get(id);
    }

    @Override
    public void update(Episode object) {
        super.update(object);
    }

    @Override
    public void updatePerservingWatchedState(Episode object) {
        if (object.getIdAndroid() != null) {
            Episode old = get(object.getIdAndroid());
            object.setWatched(old.isWatched());
        }
        if (object.getEpguideName() != null && object.getSeason() > 0 && object.getEpisode() > 0) {
            Episode old = get(object.getEpguideName(), object.getSeason(), object.getEpisode());
            if (old != null) {
                object.setWatched(old.isWatched());
                object.setIdAndroid(old.getIdAndroid());
            }
        }
        super.update(object);
    }

    @Override
    public List<Episode> get() {
        return super.getAll();
    }

    @Override
    protected String getTableName() {
        return Episode.class.getSimpleName();
    }

    @Override
    protected Episode createObject(Cursor c) {
        if (c.getPosition() < c.getCount() && c.getPosition() > -1) {
            Episode ep = new Episode();
            ep.setIdAndroid(c.getLong(c.getColumnIndex(ID)));
            ep.setEpguideName(c.getString(c.getColumnIndex(EPGUIDE_NAME)));
            ep.setEpisodeName(c.getString(c.getColumnIndex(EPISODE_NAME)));
            ep.setSeason(c.getInt(c.getColumnIndex(SEASON)));
            ep.setEpisode(c.getInt(c.getColumnIndex(EPISODE)));
            long releaseDate = c.getLong(c.getColumnIndex(RELEASE_DATE));
            long showId = c.getLong(c.getColumnIndex(SHOW_ID));
            if (showId > 0) ep.setShowId(showId);
            if (releaseDate > 0) ep.setReleaseDate(new Date(releaseDate));
            boolean watched = c.getInt(c.getColumnIndex(WATCHED)) == 1;
            ep.setWatched(watched);
            return ep;
        }
        return null;
    }

    @Override
    protected ContentValues createCV(Episode object) {
        ContentValues cv = new ContentValues();
        if (object.getIdAndroid() != null) cv.put(ID, object.getIdAndroid());
        if (object.getEpguideName() != null) cv.put(EPGUIDE_NAME, object.getEpguideName());
        if (object.getEpisodeName() != null) cv.put(EPISODE_NAME, object.getEpisodeName());
        if (object.getReleaseDate() != null)
            cv.put(RELEASE_DATE, object.getReleaseDate().getTime());
        if (object.getShowId() != null) cv.put(SHOW_ID, object.getShowId());
        cv.put(SEASON, object.getSeason());
        cv.put(EPISODE, object.getEpisode());
        cv.put(WATCHED, object.isWatched() ? 1 : 0);
        return cv;
    }

    @Override
    protected Map<String, ColumnType> buildTableColumns() {
        Map<String, ColumnType> columns = new HashMap<String, ColumnType>();
        columns.put(EPGUIDE_NAME, ColumnType.TEXT);
        columns.put(EPISODE_NAME, ColumnType.TEXT);
        columns.put(SEASON, ColumnType.INTEGER);
        columns.put(EPISODE, ColumnType.INTEGER);
        columns.put(RELEASE_DATE, ColumnType.INTEGER);
        columns.put(WATCHED, ColumnType.INTEGER);
        columns.put(SHOW_ID, ColumnType.INTEGER);
        return columns;
    }

    @Override
    public List<Episode> get(String epguideName) {
        String query = "SELECT * FROM " + getTableName() + " WHERE " + EPGUIDE_NAME + "=?";
        return super.get(query, epguideName);
    }

    @Override
    public List<Episode> get(String epguideName, int season) {
        String query = "SELECT * FROM " + getTableName() + " WHERE " + EPGUIDE_NAME + "=? AND " + SEASON + "=?";
        return super.get(query, epguideName, String.valueOf(season));
    }

    @Override
    public Episode get(String epguideName, int season, int episode) {
        String query = "SELECT * FROM " + getTableName() + " WHERE " + EPGUIDE_NAME + "=? AND " + SEASON + "=? AND " + EPISODE + "=?";
        return super.getFirst(query, epguideName, String.valueOf(season), String.valueOf(episode));
    }

    @Override
    public List<Episode> getByShow(long showId) {
        String query = "SELECT * FROM " + getTableName() + " WHERE " + SHOW_ID + "=?";
        return super.get(query, showId + "");
    }

    @Override
    public List<Episode> getByShow(long showId, int season) {
        String query = "SELECT * FROM " + getTableName() + " WHERE " + SHOW_ID + "=? AND " + SEASON + "=?";
        return super.get(query, showId + "", String.valueOf(season));
    }

    @Override
    public Episode getByShow(long showId, int season, int episode) {
        String query = "SELECT * FROM " + getTableName() + " WHERE " + SHOW_ID + "=? AND " + SEASON + "=? AND " + EPISODE + "=?";
        return super.getFirst(query, showId + "", String.valueOf(season), String.valueOf(episode));
    }

    @Override
    public boolean hasSeenAllEpisodesOfShow(long showId) {
        String query = "SELECT " + ID + " FROM " + getTableName() + " WHERE " + SHOW_ID + "=? AND " + WATCHED + "=?";
        int count = super.getCount(query, showId + "", 0 + "");
        return count < 1;
    }

    @Override
    public boolean hasAtLeastOnEpisodeSaved(long showId) {
        String query = "SELECT " + ID + " FROM " + getTableName() + " WHERE " + SHOW_ID + "=?";
        int count = super.getCount(query, showId + "");
        return count > 0;
    }

    @Override
    public List<Episode> getByReleaseDate(Date releaseDate) {
        long before = releaseDate.getTime();
        long after = before + Const.ONE_DAY_MILIS;
        String query = "SELECT * FROM " + getTableName() + " WHERE " + RELEASE_DATE + " >= ? AND " + RELEASE_DATE + " <= ?";
        return get(query, String.valueOf(before), String.valueOf(after));
    }

}
