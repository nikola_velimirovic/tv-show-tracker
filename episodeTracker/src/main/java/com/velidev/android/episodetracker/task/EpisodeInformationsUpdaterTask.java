package com.velidev.android.episodetracker.task;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.db.EpisodesDao;
import com.velidev.android.episodetracker.declaration.DataSetChangeable;
import com.velidev.android.episodetracker.object.Episode;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.util.Const;
import com.velidev.android.episodetracker.util.RestHelper;
import com.velidev.android.core.db.DatabaseProvider;

public class EpisodeInformationsUpdaterTask extends AsyncTask<JsonObject, Void, Void> {

    private EpisodesDao episodesDao;
    private Context context;
    private Show show;

    public EpisodeInformationsUpdaterTask(Context context, Show show) {
        this.context = context;
        this.show = show;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Void doInBackground(JsonObject... params) {
        for(JsonObject response : params) {
            if (response.has(Const.JSON_ERROR)) {
                Toast.makeText(App.instance, response.get(Const.JSON_ERROR).getAsString(), Toast.LENGTH_SHORT).show();
            } else {
                Iterator<Map.Entry<String, JsonElement>> keysIterator = response.entrySet().iterator();
                while (keysIterator.hasNext()) {
                    Map.Entry<String, JsonElement> entry = keysIterator.next();
                    JsonArray season = entry.getValue().getAsJsonArray();
                    List<Episode> seasonList = RestHelper.convertEpisodes(season);
                    for (Episode ep : seasonList) {
                        ep.setShowId(show.getIdAndroid());
                        getEpisodesDao().updatePerservingWatchedState(ep);
                    }
                }

//				int i = 1;
//				while(response.has(i + "")) {
//					Log.d("Reading Season", "Season " + i);
//					JSONArray seasonI = response.getJSONArray(i + "");
//					List<Episode> seasonIList = RestHelper.convertEpisodes(seasonI);
//					for(Episode ep : seasonIList) {
//						getEpisodesDao().updatePerservingWatchedState(ep);
//					}
//					i++;
//				}
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (context != null && context instanceof DataSetChangeable) {
            ((DataSetChangeable) context).notifyDataSetChanged();
        }
        super.onPostExecute(result);
    }

    private EpisodesDao getEpisodesDao() {
        if (episodesDao == null)
            episodesDao = DatabaseProvider.getInstance().getDao(EpisodesDao.class, App.getDatabase());
        return episodesDao;
    }

}
