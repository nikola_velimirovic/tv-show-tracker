package com.velidev.android.episodetracker.declaration;

public interface DataSetChangeable {
	
	public void notifyDataSetChanged();

}
