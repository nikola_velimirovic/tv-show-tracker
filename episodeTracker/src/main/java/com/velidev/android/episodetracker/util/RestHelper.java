package com.velidev.android.episodetracker.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.velidev.android.episodetracker.object.Episode;
import com.velidev.android.episodetracker.object.Show;

public class RestHelper {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public static Show convertShow(JsonObject response) {
        if (!response.has(Const.JSON_ERROR)) {
            Show show = new Show();
            if (response.has(Show.JsonField.epguide_name.name()))
                show.setEpguideName(response.get(Show.JsonField.epguide_name.name()).getAsString());
            if (response.has(Show.JsonField.imdb_id.name()))
                show.setImdbId(response.get(Show.JsonField.imdb_id.name()).getAsString());
            if (response.has(Show.JsonField.title.name()))
                show.setTitle(response.get(Show.JsonField.title.name()).getAsString());
            return show;
        }
        return null;
    }

    public static Episode convertEpisode(JsonObject response) {
        if (!response.has(Const.JSON_ERROR)) {
            Episode ep = new Episode();
            if (response.has(Episode.JsonField.season.name()))
                ep.setSeason(response.get(Episode.JsonField.season.name()).getAsInt());
            if (response.has(Episode.JsonField.number.name()))
                ep.setEpisode(response.get(Episode.JsonField.number.name()).getAsInt());
            if (response.has(Episode.JsonField.title.name()))
                ep.setEpisodeName(response.get(Episode.JsonField.title.name()).getAsString());
            if (response.has(Episode.JsonField.show.name())) {
                JsonObject show = response.get(Episode.JsonField.show.name()).getAsJsonObject();
                if (show.has(Show.JsonField.epguide_name.name()))
                    ep.setEpguideName(show.get(Show.JsonField.epguide_name.name()).getAsString());
            }
            if (response.has(Episode.JsonField.release_date.name())) {
                String releaseDateString = response.get(Episode.JsonField.release_date.name()).getAsString();
                try {
                    Date releaseDate = dateFormat.parse(releaseDateString);
                    ep.setReleaseDate(releaseDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            return ep;
        }
        return null;
    }

    public static List<Episode> convertEpisodes(JsonArray array) {
        if (array != null) {
            List<Episode> episodes = new ArrayList<Episode>();
            for (int i = 0; i < array.size(); i++) {
                JsonObject object = array.get(i).getAsJsonObject();
                Episode ep = convertEpisode(object);
                if (ep != null) episodes.add(ep);
            }
            return episodes;
        }
        return null;
    }

}
