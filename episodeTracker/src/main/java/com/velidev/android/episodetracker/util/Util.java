package com.velidev.android.episodetracker.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Nikola on 22.9.14..
 */
public class Util {

    public static Date trim(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR, 0);
        return calendar.getTime();
    }

}
