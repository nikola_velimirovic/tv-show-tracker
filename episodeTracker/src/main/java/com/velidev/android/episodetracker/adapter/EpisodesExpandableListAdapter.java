package com.velidev.android.episodetracker.adapter;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.velidev.android.episodetracker.R;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.db.EpisodesDao;
import com.velidev.android.episodetracker.dialog.MarkWatchedDialog;
import com.velidev.android.episodetracker.object.Episode;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.core.db.DatabaseProvider;
import com.velidev.android.core.util.Observable;
import com.velidev.android.core.util.Observer;

public class EpisodesExpandableListAdapter extends BaseExpandableListAdapter implements OnClickListener, Observer {
	
	public static final int OBSERVER_CODE = 1;

	private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
	private SparseArray<SparseArray<Episode>> episodes;
	private EpisodesDao episodesDao;
	private LayoutInflater inflater;
	private Show show;
	private Activity parent;
	
	public EpisodesExpandableListAdapter(Activity parent, Show show) {
		super();
		this.show = show;
		this.parent = parent;
		loadEpisodes();
		Observable.getInstance().register(this);
	}


	public void loadEpisodes() {
		if(show==null) return;
		episodes = new SparseArray<SparseArray<Episode>>();
		List<Episode> allEpisodes = getEpisodesDao().getByShow(show.getIdAndroid());
		Collections.reverse(allEpisodes);
		for(Episode ep : allEpisodes) {
			if(ep!=null) {
				SparseArray<Episode> season = episodes.get(ep.getSeason());
				if(season==null) {
					season = new SparseArray<Episode>();
					season.append(ep.getEpisode(), ep);
					episodes.append(ep.getSeason(), season);
				} else {
					season.append(ep.getEpisode(), ep);
				}
			}
		}
	}
	
	@Override
	public int getGroupCount() {
		if(episodes!=null) return episodes.size();
		return 0;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if(episodes!=null) {
			SparseArray<Episode> eps = getGroup(groupPosition);
			if(eps!=null) return eps.size();
		}
		return 0;
	}

	@Override
	public SparseArray<Episode> getGroup(int groupPosition) {
		if(episodes!=null) return episodes.get(episodes.keyAt(episodes.size()-1-groupPosition));
		return null;
	}

	@Override
	public Episode getChild(int groupPosition, int childPosition) {
		if(episodes!=null) {
			SparseArray<Episode> eps = getGroup(groupPosition);
			if(eps!=null) return eps.get(eps.keyAt(eps.size()-1-childPosition));
		}
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		if(episodes!=null) return episodes.keyAt(episodes.size()-1-groupPosition);
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		Episode child = getChild(groupPosition, childPosition);
		if(child!=null && child.getIdAndroid()!=null) return child.getIdAndroid();
		return Long.parseLong(getGroupId(groupPosition) + "" + childPosition);
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View view = createListItemView(convertView, R.layout.expandable_list_item_season);
		TextView seasonLabel = (TextView) view.findViewById(R.id.season_label);
		seasonLabel.setText("Season " + getGroupId(groupPosition));
		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		Episode ep = getChild(groupPosition, childPosition);
		View view = createListItemView(convertView, R.layout.expandable_list_item_episode);
		view.setOnClickListener(this);
		view.setTag(ep);
		View marker = view.findViewById(R.id.episode_marker);
		TextView episodeLabel = (TextView) view.findViewById(R.id.episode_title);
		episodeLabel.setText(ep.getEpisodeName());

		TextView episode = (TextView) view.findViewById(R.id.episode_number);
		episode.setText(" " + ep.getEpisode());
		
		TextView releaseDate = (TextView) view.findViewById(R.id.release_date);
		releaseDate.setText(format.format(ep.getReleaseDate()));
		
		if(ep.isWatched()) {
			marker.setBackgroundColor(this.parent.getResources().getColor(R.color.YellowGreen));
		} else if(ep.isReleased()) {
			marker.setBackgroundColor(this.parent.getResources().getColor(R.color.Tomato));
		} else {
			marker.setBackgroundColor(this.parent.getResources().getColor(R.color.Khaki));
		}
		return view;
	}
	

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
	
	private LayoutInflater getLayoutInflater() {
		if(inflater==null) inflater = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return inflater;
	}
	
	private EpisodesDao getEpisodesDao() {
		if(episodesDao==null) episodesDao = DatabaseProvider.getInstance().getDao(EpisodesDao.class, App.getDatabase());
		return episodesDao;
	}
	
	private View createListItemView(View convertView, int layout) {
		View result = null;
		if(convertView!=null) result = convertView;
		else result = getLayoutInflater().inflate(layout, null);
		return result;
	}


	@Override
	public void onClick(View v) {
		if(v.getTag()!=null && v.getTag() instanceof Episode) {
			Episode ep = (Episode) v.getTag();
			if(!ep.isReleased()) return;
			MarkWatchedDialog dialog = new MarkWatchedDialog();
			dialog.setEpisode(ep);
			dialog.show(parent.getFragmentManager(), "mark-episode");
		} else {
			Log.d("Episode click", "Tag is null or not episode");
		}
	}

	@Override
	public void update(int code, Object data) {
		if(code==OBSERVER_CODE) {
			loadEpisodes();
			notifyDataSetChanged();
		}
	}
	

}
