package com.velidev.android.episodetracker;

import android.app.ListActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.velidev.android.episodetracker.adapter.ShowsListAdapter;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.app.StartupSetup;
import com.velidev.android.episodetracker.declaration.DataSetChangeable;
import com.velidev.android.episodetracker.dialog.AddShowDialog;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.task.ShowDetailsTask;
import com.velidev.android.episodetracker.util.Const;

public class MainActivity extends ListActivity implements DataSetChangeable {
	
	private ShowsListAdapter listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initListAdapter();
		setListAdapter(listAdapter);
        new StartupSetup(this).execute();
	}

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		notifyDataSetChanged();
	}
	
	private void initListAdapter() {
		this.listAdapter = new ShowsListAdapter(this);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_show:
			AddShowDialog dialog = new AddShowDialog();
			dialog.setDataSetChangable(MainActivity.this);
			dialog.show(getFragmentManager(), "add-show-dialog");
			return true;
        case R.id.action_sync_all:
            runSynchronization();
            return true;
        }
		return super.onOptionsItemSelected(item);
	}

    private void runSynchronization() {
        Toast.makeText(this, R.string.synchronizing, Toast.LENGTH_SHORT).show();
        Show[] shows = new Show[getListAdapter().getCount()];
        for (int i = 0; i < shows.length; i++) {
            shows[i] = getListAdapter().getItem(i);
        }
        new ShowDetailsTask(this).execute(shows);
        markSynchronized();
    }

    private void markSynchronized() {
        SharedPreferences.Editor editor = getAppContext().getPreferences().edit();
        editor.putLong(Const.PROP_LAST_SYNC, System.currentTimeMillis());
        editor.apply();
    }

	@Override
	public void notifyDataSetChanged() {
		Log.d("Main Activity list", "DataSet changed notified");
		listAdapter.notifyDataChanged();
	}



    public App getAppContext() {
        return (App) getApplicationContext();
    }
    public ShowsListAdapter getListAdapter() {
        return listAdapter;
    }

}
