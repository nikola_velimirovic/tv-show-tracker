package com.velidev.android.episodetracker;

import android.annotation.TargetApi;
import android.app.ExpandableListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.gson.JsonObject;
import com.velidev.android.episodetracker.adapter.EpisodesExpandableListAdapter;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.db.ShowsDao;
import com.velidev.android.episodetracker.declaration.DataSetChangeable;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.task.ShowDetailsTask;
import com.velidev.android.episodetracker.util.Const;
import com.velidev.android.core.db.DatabaseProvider;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class ShowDetailsActivity extends ExpandableListActivity implements DataSetChangeable {

    public static final String SHOW_ID = "show_id";

    private Show show;
    private ShowsDao showsDao;
    private EpisodesExpandableListAdapter adapter;
    private ImageManager imageManager;
    private ImageTagFactory imageTagFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);
        enableHomeUp();
        loadShow(savedInstanceState);
        initShowThumb();
        initExpandableListAdapter();
        setListAdapter(adapter);
        expandAllGroupItems();
        getActionBar().setTitle(show.getTitle());
        fetchEpisodes();
    }


    private void initShowThumb() {
        ImageView thumb = (ImageView) findViewById(R.id.showThumb);
        thumb.setTag(getImageTagFactory().build(buildUrl(show), this));
        getImageManager().getLoader().load(thumb);
    }


    private void initExpandableListAdapter() {
        adapter = new EpisodesExpandableListAdapter(this, show);
    }

    private void expandAllGroupItems() {
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            getExpandableListView().expandGroup(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.show_details, menu);
        return true;
    }

    private void loadShow(Bundle savedInstanceState) {
        Long id = null;
        if (savedInstanceState != null) id = savedInstanceState.getLong(SHOW_ID);
        else id = getIntent().getLongExtra(SHOW_ID, -1);

        if (id == null || id == -1) {
            Log.w("Show", "Not found, finishing activity");
            finish();
        }
        show = getShowsDao().get(id);
        Log.d("Loaded show", show.getEpguideName());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (show != null) outState.putLong(SHOW_ID, show.getIdAndroid());
        super.onSaveInstanceState(outState);
    }

    private ShowsDao getShowsDao() {
        if (showsDao == null)
            showsDao = DatabaseProvider.getInstance().getDao(ShowsDao.class, App.getDatabase());
        return showsDao;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                handleRefresh();
                return true;
            case R.id.action_remove:
                handleRemove();
                return true;
            case R.id.action_open_imdb:
                openImdb();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openImdb() {
        Intent imdbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.imdb.com/title/" + show.getImdbId() + "/"));
        startActivity(imdbIntent);
    }

    private void handleRemove() {
        getShowsDao().delete(show);
        finish();
    }

    private void handleRefresh() {
        fetchEpisodes();
    }

    private void fetchEpisodes() {
        new ShowDetailsTask(this).execute(show);

        /*
        RestUtil.get(url, null, new JsonHttpResponseHandler(Const.UTF_8) {

			@Override
			public void onSuccess(int statusCode, JSONObject response) {
				Log.d("Success", "Code: " + statusCode);
				new EpisodeInformationsUpdaterTask(ShowDetailsActivity.this, show).execute(response);
				super.onSuccess(statusCode, response);
			}

			@Override
			public void onFailure(Throwable e, JSONObject errorResponse) {
				if(adapter.getGroupCount()==0) Toast.makeText(ShowDetailsActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
				e.printStackTrace();
				super.onFailure(e, errorResponse);
			}
		});
        String url = Const.REST_API_URL + show.getEpguideName() + "/";
        JsonObject result = RestUtil.getRestClient().getForObject(url, JsonObject.class);
        new EpisodeInformationsUpdaterTask(ShowDetailsActivity.this, show).execute(result);
        */
    }

    private ImageManager getImageManager() {
        if (imageManager == null) imageManager = App.getImageManager(this);
        return imageManager;
    }

    private ImageTagFactory getImageTagFactory() {
        if (imageTagFactory == null) imageTagFactory = App.getImageTagFactory(this);
        return imageTagFactory;
    }

    private String buildUrl(Show show) {
        return Const.SERVER_URL + show.getEpguideName() + "/cast.jpg";
    }


    @Override
    public void notifyDataSetChanged() {
        if (adapter != null) {
            adapter.loadEpisodes();
            adapter.notifyDataSetChanged();
            expandAllGroupItems();
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void enableHomeUp() {
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

}
