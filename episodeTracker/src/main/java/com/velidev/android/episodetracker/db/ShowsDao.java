package com.velidev.android.episodetracker.db;

import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.core.db.BaseDao;

public interface ShowsDao extends BaseDao<Show> {
	
	public Show get(String epguideName);
	public int count();

}
