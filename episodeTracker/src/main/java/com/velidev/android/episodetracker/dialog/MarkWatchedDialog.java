package com.velidev.android.episodetracker.dialog;

import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.Toast;

import com.velidev.android.episodetracker.R;
import com.velidev.android.episodetracker.adapter.EpisodesExpandableListAdapter;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.db.EpisodesDao;
import com.velidev.android.episodetracker.object.Episode;
import com.velidev.android.core.db.DatabaseProvider;
import com.velidev.android.core.util.Observable;

public class MarkWatchedDialog extends DialogFragment implements OnClickListener {
	
	private final static int WATCHED = 0;
	private final static int WATCHED_ALL = 1;
	private final static int NOT_WATCHED = 2;
	
	private Episode episode;
	private EpisodesDao episodesDao;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    builder.setTitle(R.string.mark_episode).setItems(R.array.mark_episode_actions, this);
	    return builder.create();
	}
	
	public void setEpisode(Episode ep) {
		this.episode = ep;
	}
	
	private EpisodesDao getEpisodesDao() {
		if(episodesDao==null) episodesDao = DatabaseProvider.getInstance().getDao(EpisodesDao.class, App.getDatabase());
		return episodesDao;
	}
	
	private void updateView() {
		Observable.getInstance().update(EpisodesExpandableListAdapter.OBSERVER_CODE, null);
	}
	
	private void markAllWatched() {
		if(episode!=null) {
			List<Episode> episodes = getEpisodesDao().get(episode.getEpguideName());
			if(episodes!=null) {
				for (Episode episode : episodes) {
					if(episode.getSeason() < this.episode.getSeason()) {
						episode.setWatched(true);
						getEpisodesDao().update(episode);
					} else if(episode.getSeason() == this.episode.getSeason() && episode.getEpisode() <= this.episode.getEpisode()) {
						episode.setWatched(true);
						getEpisodesDao().update(episode);
					}
				}
			}
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case WATCHED:
			episode.setWatched(true);
			getEpisodesDao().update(episode);
			break;
		case WATCHED_ALL:
			markAllWatched();
			break;
		case NOT_WATCHED:
			episode.setWatched(false);
			getEpisodesDao().update(episode);
			break;
		}
		Toast.makeText(getActivity(), R.string.saved, Toast.LENGTH_SHORT).show();
		updateView();
	}

}
