package com.velidev.android.episodetracker.dialog;

import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.velidev.android.episodetracker.R;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.declaration.DataSetChangeable;
import com.velidev.android.episodetracker.task.ShowAddingTask;

public class AddShowDialog extends DialogFragment implements OnClickListener {
	
	private DataSetChangeable dataSetChangable;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder
			.setView(getActivity().getLayoutInflater().inflate(R.layout.dialog_add, null))
			.setMessage(R.string.action_add_show)
			.setPositiveButton(R.string.add, this);

		return builder.create();
	}
	
	private void findEnteredShow() {
		Toast.makeText(App.instance, R.string.loading, Toast.LENGTH_SHORT).show();
		new ShowAddingTask(dataSetChangable).execute(readShowName());
	}
	
	private String readShowName() {
		EditText input = (EditText) getDialog().findViewById(R.id.showTitleInput);
		String title = input.getText().toString().replace(" ", "").toLowerCase(Locale.getDefault());
		return title;
	}

	public void setDataSetChangable(DataSetChangeable dataSetChangable) {
		this.dataSetChangable = dataSetChangable;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		findEnteredShow();
	}
	
}
