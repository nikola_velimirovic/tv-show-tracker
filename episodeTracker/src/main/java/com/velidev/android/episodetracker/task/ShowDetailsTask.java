package com.velidev.android.episodetracker.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.util.Const;
import com.velidev.android.core.util.RestUtil;

import org.springframework.web.client.ResourceAccessException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikola on 21.9.14..
 */
public class ShowDetailsTask extends AsyncTask<Show, Void, Map<JsonObject, Show>> {

    private Context context;
    private Show[] shows;

    public ShowDetailsTask(Context context) {
        super();
        this.context = context;
    }

    @Override
    protected Map<JsonObject, Show> doInBackground(Show... shows) {
        this.shows = shows;
        Map<JsonObject, Show> map = new HashMap<JsonObject, Show>();
        for (Show sh : shows) {
            String url = Const.REST_API_URL + sh.getEpguideName() + "/";
            try {
                JsonObject result = RestUtil.getRestClient().getForObject(url, JsonObject.class);
                map.put(result, sh);
            } catch (ResourceAccessException e) {
                Log.e("ShowDetailsTask", "Error fetching details, retry...", e);
                new ShowDetailsTask(context).execute(shows);
                return null;
            }
        }
        return map;
    }

    @Override
    protected void onPostExecute(Map<JsonObject, Show> map) {
        if(map==null) return;
        for (JsonObject element : map.keySet() ) {
            new EpisodeInformationsUpdaterTask(context, map.get(element)).execute(element);
        }
    }
}
