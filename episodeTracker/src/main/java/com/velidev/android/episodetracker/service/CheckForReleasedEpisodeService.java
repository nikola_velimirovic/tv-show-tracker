package com.velidev.android.episodetracker.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.velidev.android.episodetracker.R;
import com.velidev.android.episodetracker.app.App;
import com.velidev.android.episodetracker.db.EpisodesDao;
import com.velidev.android.episodetracker.db.ShowsDao;
import com.velidev.android.episodetracker.object.Episode;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.episodetracker.util.Util;
import com.velidev.android.core.db.DatabaseProvider;

import java.util.Date;
import java.util.List;

/**
 * Created by Nikola on 22.9.14..
 */
public class CheckForReleasedEpisodeService extends IntentService {

    private EpisodesDao episodesDao;
    private ShowsDao showsDao;
    private NotificationManager notificationManager;

    public CheckForReleasedEpisodeService() {
        super(TAG);
    }

    private static final String TAG = "CheckForReleasedEpisodeService";

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Running...");
        Date dateToSearch = Util.trim(new Date());
        List<Episode> episodes = getEpisodesDao().getByReleaseDate(dateToSearch);
        for(Episode ep : episodes) {
            showNotification(ep);
        }
    }

    private void showNotification(Episode ep) {
        Show show = getShowsDao().get(ep.getShowId());
        if(show!=null) {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(show.getTitle())
                            .setContentText("Released Season " + ep.getSeason() + ", Episode " + ep.getEpisode());
            getNotificationManager().notify((int)(ep.getIdAndroid()%Integer.MAX_VALUE), mBuilder.build());
        } else {
            Log.e("ReleaseDateChecker", "Show by ID " + ep.getShowId() + " not found!");
        }

    }

    private EpisodesDao getEpisodesDao() {
        if(episodesDao==null) episodesDao = DatabaseProvider.getInstance().getDao(EpisodesDao.class, App.getDatabase());
        return episodesDao;
    }
    private ShowsDao getShowsDao() {
        if(showsDao==null) showsDao = DatabaseProvider.getInstance().getDao(ShowsDao.class, App.getDatabase());
        return showsDao;
    }
    private NotificationManager getNotificationManager() {
        if(notificationManager==null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }
}
