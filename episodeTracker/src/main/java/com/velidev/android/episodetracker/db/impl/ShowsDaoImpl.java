package com.velidev.android.episodetracker.db.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;

import com.velidev.android.episodetracker.db.ShowsDao;
import com.velidev.android.episodetracker.object.Show;
import com.velidev.android.core.db.ColumnType;
import com.velidev.android.core.db.CoreDatabase;
import com.velidev.android.core.db.impl.AbstractDao;

public class ShowsDaoImpl extends AbstractDao<Show> implements ShowsDao {

	public static final String IMDB_ID = "imdbId";
	public static final String EPGUIDE_NAME = "epguideName";
	public static final String TITLE = "title";
	
	public ShowsDaoImpl(CoreDatabase db) {
		super(db);
	}

	@Override
	public void save(Show object) {
		super.insert(object);
	}

	@Override
	public void delete(Show object) {
		super.delete(object);
	}

	@Override
	public Show get(Long id) {
		return super.get(id);
	}

	@Override
	public void update(Show object) {
		super.update(object);
	}

	@Override
	public List<Show> get() {
		return super.getAll();
	}

	@Override
	public Show get(String epguideName) {
		String query = "SELECT * FROM " + getTableName() + " WHERE " + EPGUIDE_NAME + "=?";
		return super.getFirst(query, epguideName);
	}

	@Override
	public int count() {
		return super.countAll();
	}

	@Override
	protected String getTableName() {
		return Show.class.getSimpleName();
	}

	@Override
	protected Show createObject(Cursor c) {
		if(c.getPosition() < c.getCount() && c.getPosition() > -1) {
			Show show = new Show();
			show.setIdAndroid(c.getLong(c.getColumnIndex(ID)));
			show.setEpguideName(c.getString(c.getColumnIndex(EPGUIDE_NAME)));
			show.setImdbId(c.getString(c.getColumnIndex(IMDB_ID)));
			show.setTitle(c.getString(c.getColumnIndex(TITLE)));
			return show;
		}
		return null;
	}

	@Override
	protected ContentValues createCV(Show object) {
		ContentValues cv = new ContentValues();
		if(object.getIdAndroid()!=null) cv.put(ID, object.getIdAndroid());
		if(object.getEpguideName()!=null) cv.put(EPGUIDE_NAME, object.getEpguideName());
		if(object.getImdbId()!=null) cv.put(IMDB_ID, object.getImdbId());
		if(object.getTitle()!=null) cv.put(TITLE, object.getTitle());
		return cv;
	}

	@Override
	protected Map<String, ColumnType> buildTableColumns() {
		Map<String, ColumnType> columns = new HashMap<String, ColumnType>();
		columns.put(IMDB_ID, ColumnType.TEXT);
		columns.put(EPGUIDE_NAME, ColumnType.TEXT);
		columns.put(TITLE, ColumnType.TEXT);
		return columns;
	}

}
