package com.velidev.android.episodetracker.object;

import com.velidev.android.core.object.CachableObject;

public class Show extends CachableObject {
	
	private String imdbId;
	private String epguideName;
	private String title;
	
	public String getImdbId() {
		return imdbId;
	}
	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}
	public String getEpguideName() {
		return epguideName;
	}
	public void setEpguideName(String epguideName) {
		this.epguideName = epguideName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public enum JsonField {
		epguide_name,
		imdb_id,
		title
	}
	
}
