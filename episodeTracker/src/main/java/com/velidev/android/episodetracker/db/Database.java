package com.velidev.android.episodetracker.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.velidev.android.episodetracker.db.impl.EpisodesDaoImpl;
import com.velidev.android.episodetracker.db.impl.ShowsDaoImpl;
import com.velidev.android.core.db.CoreDatabase;

public class Database extends CoreDatabase {
	
	private static final int VERSION = 1;
	private static final String NAME = "episode_tracker.db";
	
	public Database(Context context) {
		super(context, NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(new ShowsDaoImpl(this).buildCreateQuery());
		db.execSQL(new EpisodesDaoImpl(this).buildCreateQuery());
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
}
