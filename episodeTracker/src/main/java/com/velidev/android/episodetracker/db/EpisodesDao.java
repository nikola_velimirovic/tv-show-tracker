package com.velidev.android.episodetracker.db;

import java.util.Date;
import java.util.List;

import com.velidev.android.episodetracker.object.Episode;
import com.velidev.android.core.db.BaseDao;

public interface EpisodesDao extends BaseDao<Episode> {
	
	public void updatePerservingWatchedState(Episode object);
	public List<Episode> get(String epguideName);
	public List<Episode> get(String epguideName, int season);
	public Episode get(String epguideName, int season, int episode);
	public List<Episode> getByShow(long showId);
	public List<Episode> getByShow(long showId, int season);
	public Episode getByShow(long showId, int season, int episode);
	public boolean hasSeenAllEpisodesOfShow(long showId);
	public boolean hasAtLeastOnEpisodeSaved(long showId);
    public List<Episode> getByReleaseDate(Date releaseDate);
	
}
