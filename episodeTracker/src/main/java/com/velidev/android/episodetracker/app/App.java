package com.velidev.android.episodetracker.app;

import com.velidev.android.episodetracker.db.Database;
import com.velidev.android.episodetracker.db.EpisodesDao;
import com.velidev.android.episodetracker.db.ShowsDao;
import com.velidev.android.episodetracker.db.impl.EpisodesDaoImpl;
import com.velidev.android.episodetracker.db.impl.ShowsDaoImpl;
import com.velidev.android.core.app.AbstractApp;
import com.velidev.android.core.app.Binder;

public class App extends AbstractApp {
	
	private static Database database;

	@Override
	protected void bind(Binder binder) {
		binder.bind(ShowsDao.class, ShowsDaoImpl.class);
		binder.bind(EpisodesDao.class, EpisodesDaoImpl.class);
	}
	
	public static Database getDatabase() {
		if(database==null) database = new Database(instance);
		return database;
	}

}
